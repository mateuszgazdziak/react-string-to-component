/**
 * Splits string with params into array
 *
 * @param {string} inputString
 *
 * @returns {Array}
 *
 * @example
 * input: 'The prime site {siteName} has been successfully removed with all remote sites.'
 * output: [
 *   'The prime site ',
 *   '{siteName}',
 *   ' has been successfully removed with all remote sites.',
 *]
 */

export const splitStringWithParamsIntoArray = inputString => {
    const result = [];

    let currentIndex = 0;
    while (currentIndex < inputString.length) {
        const openingBracketIndex = inputString.indexOf('{', currentIndex);
        const closingBracketIndex = inputString.indexOf('}', currentIndex + 1);

        if (openingBracketIndex !== -1 && closingBracketIndex !== -1) {
            result.push(inputString.slice(currentIndex, openingBracketIndex));
            result.push(
                inputString.slice(openingBracketIndex, closingBracketIndex + 1),
            );

            currentIndex = closingBracketIndex + 1;
        } else {
            result.push(inputString.slice(currentIndex, inputString.length));
            currentIndex = inputString.length;
        }
    }

    return result;
};

/**
 * Replaces params in string with components
 *
 * @param {String} inputString
 * @param {Object} componentMapping
 * @property {sting} component name
 * @property {React.Component} component
 *
 * @returns {Array}
 *
 * @example
 * input: {
 *   inputString: 'Test {button} and {div}',
 *   componentMapping: { button: <button />, div: <div /> },
 * }
 * output: ['Test ', <button />, ' and ', <div />]
 */

export const replaceStringWithComponent = ({
    inputString,
    componentMapping,
}) => {
    const argumentsRegExp = /\{[^}]*\}/g;
    const argNames = inputString.match(argumentsRegExp);
    const componentNames = Object.keys(componentMapping);

    if (argNames.length !== componentNames.length) {
        throw new Error(
            'replaceStringWithComponent - number of passed components doest match with arguments found in string',
        );
    }

    const splittedString = splitStringWithParamsIntoArray(inputString);
    const result = splittedString;

    componentNames.forEach(component => {
        const indexOfComponent = result.indexOf(`{${component}}`);

        if (indexOfComponent === -1) {
            throw new Error(
                `replaceStringWithComponent - component: ${component} not found in string`,
            );
        }
        result[indexOfComponent] = componentMapping[component];
    });

    return result;
};
